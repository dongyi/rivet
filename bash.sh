#!/bin/bash
singularity run --bind /sdf/group/ldmx/users/dongyi/data/test:/sdf/group/ldmx/users/dongyi/data/test/,/cvmfs:/cvmfs/,/sdf/group/ldmx/users/dongyi/data/NWF /sdf/home/d/dliu64/madgraph_latest-3.5.1pythia8.310.sif
setupATLAS
asetup AthGeneration, 23.6.2
source setupRivet
export RIVET_ANALYSIS_PATH=/sdf/group/ldmx/users/dongyi/data/NWF
cd /sdf/group/ldmx/users/dongyi/data/NWF
rivet-build MY_NWF.cc
